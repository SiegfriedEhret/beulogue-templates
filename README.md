# beulogue templates

This repository contains multiple templates to start a website using [`beulogue`](https://beulogue.ehret.me/).

## Single language

This is a template for a single language website.

It is available on:

- [bitbucket](https://bitbucket.org/siegfriedehret/beulogue-templates/src/single-language/)
- [codeberg](https://codeberg.org/SiegfriedEhret/beulogue-templates/src/branch/single-language)
- [github](https://github.com/SiegfriedEhret/beulogue-templates/tree/single-language)


## Multiple languages

This is a template for a multiple languages website.

It is available on:

- [bitbucket](https://bitbucket.org/siegfriedehret/beulogue-templates/src/multiple-languages/)
- [codeberg](https://codeberg.org/SiegfriedEhret/beulogue-templates/src/branch/multiple-languages)
- [github](https://github.com/SiegfriedEhret/beulogue-templates/tree/multiple-languages)
